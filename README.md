Dotfiles
===================

    > git clone https://gitlab.com/mhamon/dotfiles.git
    > cd dotfiles
    > ./install_dotfiles.sh

### Dependencies
This script need vim, git and stow packages
